<?php

namespace App\Http\Controllers;

use App\Models\profile;
use App\Models\user;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function daftar()
    {
        return view('page.register');
    }

    public function kirim(Request $request)
    {
        $namaDepan = $request->input('fname');
        $namaBelakang = $request->input('lname');

        return view('page.welcome', ['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
    }
    public function profile()
    {
        $user = Auth::user();
        $profile = profile::where('user_id', $user->id)->first();
        return view('page.profile', compact('profile'));
    }
}
