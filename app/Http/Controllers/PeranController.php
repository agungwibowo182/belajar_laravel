<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Peran;

class PeranController extends Controller
{
    public function index()
    {
        $peran = Peran::all();
        return view('peran.index', compact('peran'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('peran.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'film_id' => 'required',
            'cast_id' => 'required',
            'nama' => 'required'
        ]);

        // Post::create([
        //     'title' => $request->title,
        //     'body' => $request->body
        // ]);

        $peran = new Peran();
        $peran->film_id = $request->film_id;
        $peran->cast_id = $request->cast_id;
        $peran->nama = $request->nama;
        $peran->save();

        return redirect('/peran');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $peran = Peran::find($id);
        return view('peran.show', compact('peran'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $peran = Peran::find($id);
        return view('peran.edit', compact('peran'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'film_id' => 'required',
            'cast_id' => 'required',
            'nama' => 'required'
        ]);

        $peran = Peran::find($id);
        $peran->film_id = $request->film_id;
        $peran->cast_id = $request->cast_id;
        $peran->nama = $request->nama;
        $peran->update();
        return redirect('/peran');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $peran = Peran::find($id);
        $peran->delete();
        return redirect('/peran');
    }
}
