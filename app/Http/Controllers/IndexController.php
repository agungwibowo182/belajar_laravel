<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function utama()
    {
        return view('page');
    }

    public function dataTable()
    {
        return view('page.data-table');
    }
}
