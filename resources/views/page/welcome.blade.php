@extends('layouts.master')

@section('title')
Halaman Index 
@endsection

@section('content')

    <h1>SELAMAT DATANG! {{$namaDepan}} {{$namaBelakang}}</h1>
    <h4>Terima kasih telah bergabung di Website Kami. Media Belajar kita bersama!</h4>
    @yield('content')
@endsection