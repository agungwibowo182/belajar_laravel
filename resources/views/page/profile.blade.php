@extends('layouts.master')

@section('title','Halaman Profile')

@section('content')
<table class="table table-hover text-nowrap">
    <tbody>
      <tr>
        <th>Nama :</th>
        <td>{{$profile->user->name}}</td>
      </tr>
      <tr>
        <th>Email :</th>
        <td>{{$profile->user->email}}</td>
      </tr>
      <tr>
        <th>Umur :</th>
        <td>{{$profile->umur}}</td>
      </tr>
      <tr>
        <th>Bio :</th>
        <td>{{$profile->bio}}</td>
      </tr>
      <tr>
        <th>Alamat :</th>
        <td>{{$profile->alamat}}</td>
      </tr>

    </tbody>
  </table>
@endsection
