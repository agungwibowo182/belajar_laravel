@extends('layouts.master')

@section('title')
Halaman Form 
@endsection

@section('content')
    <h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST"> 
        @csrf
        <label>First Name :</label> <br><br>
        <input type="text" name="fname"> <br><br>
        <label>Last Name :</label> <br><br>
        <input type="text" name="lname"> <br><br>
        
        <label>Gender</label> <br><br>
        <input type="radio" id="male" name="gender" value="Male">
        <label>Male</label><br>
        <input type="radio" id="female" name="gender" value="Female">
        <label>Female</label><br><br>
     
        <label>Nationality</label> <br><br>
        <select name="nationality">
            <option value="indonesia">Indonesia</option>
            <option value="amerika">Amerika</option>
            <option value="inggris">Inggris</option>
        </select>
        <br><br>

        <label>Language Spoken</label><br><br>
        <input type="checkbox" name="indonesia">Bahasa Indonesia<br>
        <input type="checkbox" name="english">English<br>
        <input type="checkbox" name="other">Other<br>
        <br><br>

        <label>Bio</label><br><br>
        <textarea name="pesan" rows="10" cols="30"></textarea>
        <br>
        <input type="submit" name="submit" value="Sign Up">

    </form>
@endsection
