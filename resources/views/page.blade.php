@extends('layouts.master')

@section('title')
Halaman Pertama 
@endsection

@section('content')

    <h1>Media Online</h1>
    <h2>Sosial Media Developer</h2>
    <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
    <h3>Benefit Join di Media Online</h3>
    <ul>
        <li>Mendapatan motivasi dari sesama Developer</li>
        <li>Sharing knowlenge</li>
        <li>Dibuat oleh calon web developer terbaik</li>
    </ul>
    <h3>Cara Bergabung ke Media Online</h3>
    <ol>
        <li>Menguji Website ini</li>
        <li>Mendaftarkan di <a href = "{{route('register')}}">Form Sign Up</a></li>
        <li>Selesai</li>
    </ol>
@endsection
    

</body>
</html>