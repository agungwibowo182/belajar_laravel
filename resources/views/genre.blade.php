@extends('layouts.master')
@section('judul')
Genre
@endsection

@section('content')
<h3>One to Many</h3>
<table>
  <thead>
   <tr>
    <th>genre</th>
    <th>judul</th>
   </tr>
  <tbody>
   @foreach($genre as $value)
   <tr>
    <td>{{$value->nama}}</td>
    <td>
       @foreach($value->film as $f)
       {{$f->judul}}
       @endforeach
    </td>
   </tr>
   @endforeach
  <tbody>
  </thead>
</table>
@endsection