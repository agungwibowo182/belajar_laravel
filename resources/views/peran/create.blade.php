@extends('layouts.master')

@section('title','Tambah Data')

@section('content')

    <form action="/peran" method="POST">
        @csrf
        <div class="form-group">
            <label for="film_id">Film</label>
            <input type="text" class="form-control" name="film_id" id="film_id" placeholder="Masukkan film_id">
            @error('film_id')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="cast_id">Cast</label>
            <input type="number" class="form-control" name="cast_id" id="cast_id" placeholder="Masukkan cast_id">
            @error('cast_id')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan nama">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
@endsection

