@extends('layouts.master')

@section('title')
Edit Peran {{$peran->id}}
@endsection

@section('content') 
<div>
    <form action="/peran/{{$peran->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="film_id">Film</label>
            <input type="text" class="form-control" name="film_id" value="{{$peran->film_id}}" id="film_id" placeholder="Masukkan film_id">
            @error('film_id')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="cast_id">Cast</label>
            <input type="number" class="form-control" name="cast_id"  value="{{$peran->cast_id}}"  id="cast_id" placeholder="Masukkan cast_id">
            @error('cast_id')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" class="form-control" name="nama"  value="{{$peran->nama}}"  id="nama" placeholder="Masukkan nama">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
</div>
@endsection

