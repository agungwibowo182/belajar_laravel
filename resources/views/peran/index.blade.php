@extends('layouts.master')

@section('title')
List Data Peran
@endsection

@section('content')

<a href="/peran/create" class="btn btn-primary mb-3">Tambah</a>
    <table class="table">
        <thead class="thead-light">
            <tr>
            <th scope="col">#</th>
            <th scope="col">Film</th>
            <th scope="col">Cast</th>
            <th scope="col">Nama</th>
            <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($peran as $key=>$value)
                <tr>
                    <td>{{$key + 1}}</th>
                    <td>{{$value->film_id}}</td>
                    <td>{{$value->cast_id}}</td>
                    <td>{{$value->nama}}</td>
                    <td>
                        <a href="/peran/{{$value->id}}" class="btn btn-info">Show</a>
                        <a href="/peran/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                        <form action="/peran/{{$value->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" class="btn btn-danger my-1" onclick="return confirm('Apakah anda yakin menghapus data {{$value->nama}}?')" value="Delete">
                        </form>
                    </td>
                </tr>
            @empty
                <tr colspan="3">
                    <td>No data</td>
                </tr>  
            @endforelse              
        </tbody>
    </table>
@endsection

