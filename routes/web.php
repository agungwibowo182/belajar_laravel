<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\PeranController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\KritikController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|

route => mengatur jalur
view => tampilan dengan ext .blade.php 
controller => function belakang layar PHP
model => Database


*/

Route::get('/', [IndexController::class, 'utama'])->name('home');
Route::get('/register', [AuthController::class, 'daftar'])->name('register')->middleware('auth');
Route::POST('/welcome', [AuthController::class, 'kirim'])->name('home')->middleware('auth');

Route::get('/data-table', [IndexController::class, 'dataTable'])->name('home')->middleware('auth');

//cast
Route::get('/cast', [CastController::class, 'index']);
Route::get('/cast/create', [CastController::class, 'create']);
Route::post('/cast', [CastController::class, 'store']);
Route::get('/cast/{cast_id}', [CastController::class, 'show']);
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
Route::put('/cast/{cast_id}', [CastController::class, 'update']);
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);

Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/profile', [AuthController::class, 'profile'])->middleware('auth');


//peran
Route::get('/peran', [PeranController::class, 'index']);
Route::get('/peran/create', [PeranController::class, 'create']);
Route::post('/peran', [PeranController::class, 'store']);
Route::get('/peran/{peran_id}', [PeranController::class, 'show']);
Route::get('/peran/{peran_id}/edit', [PeranController::class, 'edit']);
Route::put('/peran/{peran_id}', [PeranController::class, 'update']);
Route::delete('/peran/{peran_id}', [PeranController::class, 'destroy']);


Route::get('user', [UserController::class, 'index']);
Route::get('genre', [GenreController::class, 'index']);
